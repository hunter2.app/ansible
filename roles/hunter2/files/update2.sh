#!/bin/bash
set -euo pipefail

cd "$(dirname "${0}")"

. ./.env

error() {
        [[ -n "${H2_DISCORD_URL}" ]] && curl -H "Content-Type: application/json" -X POST -d "{\"content\": \"Failed to update from ${old_version} to ${new_version}\"}" "${H2_DISCORD_URL}"
}

trap 'error' ERR

old_version="${H2_IMAGE_VERSION}"
git pull
new_version="$(git describe --abbrev=8)"
if [[ "${old_version}" == "${new_version}" ]]
then
        exit 0
fi
sed -i -e "/^H2_IMAGE_VERSION=/c H2_IMAGE_VERSION=${new_version}" .env
docker compose pull
docker compose run --rm websocket migrate_schemas
docker compose up -d
